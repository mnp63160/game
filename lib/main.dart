import 'package:device_preview/device_preview.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:game_math/level3.dart';
import 'package:game_math/level4.dart';
import 'package:game_math/level5.dart';
import 'package:game_math/providerScore.dart';
import 'package:game_math/win.dart';
import 'package:provider/provider.dart';
import 'lose.dart';
import 'start.dart';
import 'level1.dart';
import 'level2.dart';
import 'timeOver.dart';

void main() {
  runApp(DevicePreview(
    enabled: true,
    builder: (BuildContext context) => const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => ProviderScore(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.cyan,
          ),
          // home: test(),
          home: Start(),
        ));
  }
}

// class test extends StatelessWidget {
//   const test({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('main'),
//         ),
//         body: Container(
//             padding: EdgeInsets.only(top: 20, left: 20, right: 20),
//             alignment: Alignment.topCenter,
//             child: Column(
//               children: [
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Start(),
//                           ));
//                     },
//                     child: Text("start")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Win(),
//                           ));
//                     },
//                     child: Text("Win")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Lose(),
//                           ));
//                     },
//                     child: Text("Lose")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Level1(),
//                           ));
//                     },
//                     child: Text("1")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Level2(),
//                           ));
//                     },
//                     child: Text("2")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Level3(),
//                           ));
//                     },
//                     child: Text("3")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Level4(),
//                           ));
//                     },
//                     child: Text("4")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => Level5(),
//                           ));
//                     },
//                     child: Text("5")),
//                 ElevatedButton(
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => timeOver(),
//                           ));
//                     },
//                     child: Text("time over")),
                    
//               ],
//             )
//             )
//             );
//   }
// }
