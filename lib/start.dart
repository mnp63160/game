import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'level1.dart';

class Start extends StatefulWidget {
  const Start({Key? key}) : super(key: key);
  @override
  State<Start> createState() => _Start();
}

class _Start extends  State<Start> {
  // const Start({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 255, 139, 253),
        body: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              start(), 
              imageStart2()
            ]
          )
        );
  }

  Widget start() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(
              height: 40,
            ),
            // title(),
            title2(),
            btnPlay(),
            SizedBox(
              height: 70,
            ),
          ]),
        ],
      ),
    );
  }

  Widget btnPlay() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
          builder: (context) => Level1(),
        ));
      },
      child: Container(
        height: 60,
        width: 170,
        decoration: BoxDecoration(
            color: Color.fromARGB(255, 36, 44, 196),
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                  color: Color.fromARGB(255, 2, 9, 132),
                  spreadRadius: 1,
                  blurRadius: 8,
                  offset: Offset(4, 4)),
              // BoxShadow(
              //   color: Color.fromARGB(255, 255, 255, 255),
              //   spreadRadius: 1,
              //   blurRadius: 8,
              //   offset: Offset(-4,-4)
              // ),
            ]),
        child: Center(
            child: Text(
          "PLAY",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30),
        )),
      ),
    );
  }

  Widget title2() {
    return Container(
      alignment: FractionalOffset.bottomCenter,
      width: 300.0,
      child: TextLiquidFill(
        text: 'Hi-Speed\n   Math',
        // waveDuration: Duration(seconds: 2),
        waveColor: Colors.blueAccent,
        boxBackgroundColor: Color.fromARGB(255, 255, 139, 253),
        textStyle: TextStyle(
          fontSize: 70.0,
          fontWeight: FontWeight.bold,
        ),
        boxHeight: 200.0,
      ),
    );
  }

  Widget title() {
    const colorizeColors = [
      Colors.purple,
      Colors.blue,
      Colors.yellow,
      Colors.red,
    ];

    const colorizeTextStyle = TextStyle(
      fontSize: 80.0,
      fontFamily: 'TiltWarp',
    );
    return SizedBox(
      width: 500.0,
      child: Center(
        child: AnimatedTextKit(
          animatedTexts: [
            ColorizeAnimatedText(
              'Hi-Speed',
              textStyle: colorizeTextStyle,
              colors: colorizeColors,
            ),
            ColorizeAnimatedText(
              'Math',
              textStyle: colorizeTextStyle,
              colors: colorizeColors,
            ),
          ],
          isRepeatingAnimation: true,
          onTap: () {
            print("Tap Event");
          },
        ),
      ),
    );
  }

  Widget imageStart() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        'assets/images/characterStart.png',
        fit: BoxFit.fitHeight,
      ),
    );
  }

  Widget imageStart2() {
    return Container(
      alignment: FractionalOffset.bottomCenter,
      child: Image.asset(
        'assets/images/characterStart.png',
        fit: BoxFit.fitHeight,
      ),
    );
  }
}
