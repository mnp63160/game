import 'package:flutter/material.dart';
import 'package:game_math/providerScore.dart';
import 'package:game_math/start.dart';
import 'package:provider/provider.dart';

import 'main.dart';

class Lose extends StatelessWidget {
  const Lose({super.key});

  @override
  Widget build(BuildContext context) {
    final ProviderScore counter = Provider.of<ProviderScore>(context);
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 90, 230, 232),
      body: ListView(padding: EdgeInsets.zero, children: <Widget>[
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Container(
              height: 100,
            ),
          ]),
          Row(children: <Widget>[
            Container(
                height: 100,
                width: 150,
                child: Image.asset(
                  "assets/images/1.png",
                  fit: BoxFit.fitHeight,
                )),
            Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                'ลุงว่าผิดมั้ย',
                style: TextStyle(
                  fontSize: 15,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
              ),
            ),
          ]),
        ]),
        Column(crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
          // Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          //   Container(
          //     height: 50,
          //   ),
          // ]),
          Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                'ผิดแหละ',
                style: TextStyle(
                  fontSize: 15,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
              ),
            ),
            Container(
                height: 100,
                width: 150,
                child: Image.asset(
                  "assets/images/4.png",
                  fit: BoxFit.fitHeight,
                )),
          ]),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      // Container(
                      //   height: 50,
                      // ),
                    ]),
                Row(children: <Widget>[
                  Container(
                      height: 100,
                      width: 150,
                      child: Image.asset(
                        "assets/images/3.png",
                        fit: BoxFit.fitHeight,
                      )),
                  Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 255, 255, 255),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                      'ทำได้ ${counter.num} คะแนนก็เก่งแล้ว',
                      style: TextStyle(
                        fontSize: 15,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                    ),
                  ),
                ]),
              ]),
          Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              SizedBox(
                width: 20,
                height: 20,
              ),
              SizedBox(
                width: 80,
                height: 80,
                child: FittedBox(
                  child: FloatingActionButton(
                    backgroundColor: Color.fromARGB(255, 255, 64, 255),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MyApp(),
                          ));
                    },
                    child: Icon(
                      Icons.home,
                      size: 20,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 20,
                height: 20,
              ),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              SizedBox(
                width: 20,
                height: 20,
              ),
            ])
          ]),
        ])
      ]),
    );
  }
}
