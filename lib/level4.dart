import 'dart:async';

import 'package:flutter/material.dart';
import 'package:game_math/level5.dart';
import 'package:game_math/lose.dart';
import 'package:game_math/providerScore.dart';
import 'package:game_math/start.dart';
import 'package:game_math/timeover.dart';
import 'package:game_math/win.dart';
import 'package:provider/provider.dart';

import 'main.dart';

class Level4 extends StatefulWidget {
  const Level4({Key? key}) : super(key: key);
  @override
  State<Level4> createState() => _Level4();
}

class _Level4 extends State<Level4> {
  static const maxSeconds = 10;
  int count = maxSeconds;
  Timer? _timer;
  String userInput = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 90, 230, 232),
        body: Stack(
            alignment: AlignmentDirectional.center,
            fit: StackFit.loose,
            children: [level4(), levelLetgo()]));
  }

  bool _isShowlevel4 = false;
  bool _isShowLetGo = true;
  Widget level4() {
    return Visibility(
        visible: _isShowlevel4,
        child: Container(
            color: Color.fromARGB(255, 90, 230, 232),
            child: ListView(padding: EdgeInsets.zero, children: <Widget>[
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 45,
                    ),
                    changeLevel(),
                    SizedBox(
                      height: 16.5,
                    ),
                    time(),
                  ]),
              SizedBox(
                height: 35,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    problemLv4(),
                    SizedBox(
                      height: 33,
                    ),
                  ]),
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    input(userInput),
                    SizedBox(
                      height: 20,
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    numberpad("1"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpad("2"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpad("3"),
                  ]),
              SizedBox(
                height: 10,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    numberpad("4"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpad("5"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpad("6"),
                  ]),
              SizedBox(
                height: 10,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    numberpad("7"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpad("8"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpad("9"),
                  ]),
              SizedBox(
                height: 10,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    numberpad("0"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpadCancle("C"),
                    SizedBox(
                      width: 20,
                    ),
                    numberpadOk("✔"),
                  ]),
              SizedBox(
                height: 25,
              ),
              value(),
            ])));
  }

  Widget time() {
    // int seconds = 10;
    // Timer? timer;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Time :",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          width: 15,
        ),
        Container(
          height: 35,
          width: 70,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
            borderRadius: BorderRadius.circular(30),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              '$count',
              style: TextStyle(fontSize: 20),
            ),
          ),
        ),
        // IconButton(
        //   iconSize: 20,
        //   icon: const Icon(Icons.favorite),
        //   onPressed: () {
        //     startTimer();
        //   },
        // ),
      ],
    );
  }

  startTimer() {
    count = 10;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (count > 0) {
        setState(() {
          count--;
        });
      } else {
        stopTimer();
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => timeOver(),
            ));
      }
    });
  }

  void stopTimer() {
    _timer?.cancel();
  }

  Widget changeLevel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 10,
          width: 45,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 139, 253),
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          height: 10,
          width: 45,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 139, 253),
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          height: 10,
          width: 45,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 139, 253),
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          height: 10,
          width: 45,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 139, 253),
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          height: 10,
          width: 45,
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(30),
          ),
        ),
      ],
    );
  }

  Widget problemLv4() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            width: 120,
            child: Image.asset(
              "assets/images/1.png",
              fit: BoxFit.fitHeight,
            )),
        Container(
            width: 120,
            child: Image.asset(
              "assets/images/1.png",
              fit: BoxFit.fitHeight,
            )),
        Text(
          "+",
          style: TextStyle(fontSize: 50),
        ),
        Container(
            width: 120,
            child: Image.asset(
              "assets/images/4.png",
              fit: BoxFit.fitHeight,
            )),
      ],
    );
  }

  Widget numberpad(String text) {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          handleButtonPress(text);
        });
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromARGB(255, 21, 58, 168),
        fixedSize: const Size(60, 60),
        shape: const CircleBorder(),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 30, color: Colors.white),
      ),
    );
  }

  handleButtonPress(String text) {
    userInput = userInput + text;
  }

  handleButtonPressC() {
    userInput = userInput.substring(0, userInput.length - 1);
    return;
  }

  Widget numberpadCancle(String text) {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          handleButtonPressC();
        });
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromARGB(255, 242, 112, 205),
        fixedSize: const Size(60, 60),
        shape: const CircleBorder(),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 30, color: Colors.white),
      ),
    );
  }

  Widget numberpadOk(String text) {
    final ProviderScore counter = Provider.of<ProviderScore>(context);
    return ElevatedButton(
      onPressed: () {
        if (userInput != "") {
          if (userInput == "6") {
            counter.increaseNumber();
            stopTimer();
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Level5(),
                ));
          } else {
            stopTimer();
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Lose(),
                ));
          }
        }
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromARGB(255, 52, 246, 94),
        fixedSize: const Size(60, 60),
        shape: const CircleBorder(),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 30, color: Colors.white),
      ),
    );
  }

  Widget input(String userInput) {
    return Column(
      children: [
        Container(
          height: 75,
          width: 300,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 0, 0, 0),
            borderRadius: BorderRadius.circular(50),
          ),
          alignment: Alignment.center,
          child: Text(
            userInput,
            style: const TextStyle(
                fontSize: 48, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ),
      ],
    );
  }

  Widget value() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.all(10),
        color: Color.fromARGB(255, 255, 139, 253),
        height: 185,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/1.png',
                  fit: BoxFit.fitHeight,
                  width: 80,
                ),
                Text(
                  "= 1",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 20,
                ),
                Image.asset(
                  'assets/images/2.png',
                  fit: BoxFit.fitHeight,
                  width: 80,
                ),
                Text(
                  "= 2",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/3.png',
                  fit: BoxFit.fitHeight,
                  width: 80,
                ),
                Text(
                  "= 3",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 20,
                ),
                Image.asset(
                  'assets/images/4.png',
                  fit: BoxFit.fitHeight,
                  width: 80,
                ),
                Text(
                  "= 4",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget levelLetgo() {
    return Visibility(
      visible: _isShowLetGo,
      child: Padding(
        padding: const EdgeInsets.only(top: 100),
        // padding: const EdgeInsets.all(50),
        child: Container(
          alignment: AlignmentDirectional.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Level',
                style: TextStyle(
                  fontSize: 70,
                  fontWeight: FontWeight.bold,
                  // color: Colors.white
                ),
              ),
              Text(
                '4',
                style: TextStyle(
                  fontSize: 100,
                  fontWeight: FontWeight.bold,
                  // color: Colors.white
                ),
              ),
              SizedBox(
                height: 20,
              ),
              btnLetgo()
            ],
          ),
        ),
      ),
    );
  }

  Widget btnLetgo() {
    return GestureDetector(
      onTap: () {
        startTimer();
        setState(
          () {
            _isShowLetGo = !_isShowLetGo;
          },
        );
        setState(
          () {
            _isShowlevel4 = !_isShowlevel4;
          },
        );
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //   builder: (context) => Level1(),
        // ));
      },
      child: Container(
        alignment: AlignmentDirectional.center,
        height: 70,
        width: 180,
        decoration: BoxDecoration(
            color: Color.fromARGB(255, 36, 44, 196),
            borderRadius: BorderRadius.circular(50),
            boxShadow: [
              BoxShadow(
                  color: Color.fromARGB(255, 2, 9, 132),
                  spreadRadius: 1,
                  blurRadius: 8,
                  offset: Offset(4, 4)),
            ]),
        child: Center(
            child: Text(
          "Let's go!",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 35),
        )),
      ),
    );
  }
}
